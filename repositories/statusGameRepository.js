const gamers = new Map();
const finishedGamers = new Set();

const calcProcess = (gamer) => {
    const textLength = gamer.gameText.length;
    const position = gamer.currentKeyPosition;
    const textRestlength = textLength - (textLength - position);
    const result = (100 * textRestlength)/ textLength;
    return result;
};

export const getGamers = () => gamers.values();

export const setGamer = (member) => {
    const newGamer = {
        ...member,
        currentKeyPosition: 0,
        process: 0,
        isFinished: false
    };
    
    const {username} = newGamer;
    gamers.set(username, newGamer);
    return newGamer;
};

export const setFinishedGamer = (username) => {
    const gamer = gamers.get(username);
    gamer.isFinished = true;
    gamers.set(username, gamer);
    finishedGamers.add(username);
};

export const getFinishedUsersArr = () => Array.from(finishedGamers);

export const selectKey = (username, key) => {
    const gamer = gamers.get(username);
    const { gameText, currentKeyPosition } = gamer;
    if (gameText.charAt(currentKeyPosition) === key) {
        gamer.currentKeyPosition = currentKeyPosition + 1;
        gamer.process = calcProcess(gamer);
        gamers.set(username, gamer);
    };  
    return gamer;
};

export const clearGamers = () => {
    gamers.clear();
    finishedGamers.clear();
};
