const rooms = new Map();

const updateMemberStatus = (membersSet, username) => {
    let membersArr = Array.from(membersSet);
    membersArr = membersArr.map(member => {
        if (member.username === username) {
            member.isReady = !member.isReady;
        }
        return member;
    });
    return new Set(membersArr);
};

const deleteMember = (membersSet, username) => {
    let membersArr = Array.from(membersSet);
    membersArr = membersArr.filter(member => member.username !== username);
    return new Set(membersArr);
};

export const addRoom = ({roomData, members}) =>  rooms.set(roomData.roomName, {roomData, members});

export const deleteRoom = roomName => rooms.delete(roomName);

export const addUserToRoom = (roomName, username) => {
    const user = {
        username,
        isReady: false
    };
    const room = rooms.get(roomName);
    room.members.add(user);
    rooms.set(roomName, room);
}

export const deleteUserToRoom = (roomName, username) => {
    const room = rooms.get(roomName);
    const { members } = room;
    room.members = deleteMember(members, username);
};

export const getRoom = roomName => rooms.get(roomName);

export const getRoomMembers = roomName => {
    const { members } = rooms.get(roomName);
    return Array.from(members);
};

export const getCurrentRoomName = socket => Object.keys(socket.rooms).find(roomName => rooms.has(roomName));

export const getRooms = () => rooms;

export const updateUserStatusInRoom = (activeRoomName, username) => {
    const room = rooms.get(activeRoomName);
    const { members } = room;
    room.members = updateMemberStatus(members, username);
    return room.members;
};

export const isExistRoom = (roomName) => {
    const room = rooms.get(roomName);
    return Boolean(room);
};
