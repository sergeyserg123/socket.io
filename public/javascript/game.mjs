import { addClass, removeClass } from "./helper.mjs";
import { createCard, createUserContainer, createText, createTimerText, createMarkedText } from "./elementsHelper.mjs";
import { createGameTimer } from "./elementsHelper.mjs";


let rooms = [];
let members = [];
let activeRoomName = null;
let isGameStarted = false;

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const socket= io("http://localhost:3002/game");

const roomsPage = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");
const userList = document.getElementById('user-list');
const gameArea = document.getElementById('game-area');
const createButton = document.getElementById("create-button");
const leaveButton = document.getElementById("leave-button");
const readyButton = document.getElementById("game-text");
const cardsContainer = document.getElementById("cards-container");

const setActiveRoomName = roomName => {
  activeRoomName = roomName;
};

const onCreateRoom = () => {
  const roomName = prompt();
  if (roomName) {
    const roomData = {
      roomName
    };
    socket.emit('CREATE_ROOM', roomData);
  }
};

// TODO: delete room
const onDeleteRoom = () => {
  socket.emit('DELETE_ROOM', roomName);
};

const onSelectRoom = (e) => {
  if(e.target.tagName === 'BUTTON') {
    e.preventDefault();
    const roomName = e.target.id;
    socket.emit('JOIN_ROOM', {roomName, username});
  };
};

const onKeyPress = (e) => {
  e.preventDefault();
  if (isGameStarted) {
    const { key } = e;
    socket.emit('KEY_PRESS', {username, key});
  }
};

const updateRooms = (query) => {
  const roomData = {...query};
  rooms.push(roomData);
  const card = createCard(roomData);
  cardsContainer.append(card);
};

const existRoom = (roomData) => {
  const { roomName } = roomData;
  alert(`Room ${roomName} already exist!`);
};

const fullRoom = (roomName) => {
  alert(`Room ${roomName} is FULL! Please, select another one.`);
};

const clearGameUserList = () => {
  while (userList.firstChild) {
    userList.firstChild.remove();
  };
};

const clearTimer = () => {
  const gameTimerSpan = document.getElementById("game-timer");
  gameArea.removeChild(gameTimerSpan);
};

const fillGameUserList = () => {
  members.forEach(member => {
    const userContainer = createUserContainer(member);
    userList.append(userContainer);
  }); 
};

const updateRoomsList = (query) => {
  const roomData = query.map(el => el.roomData);
  rooms = [...roomData];
  rooms.forEach(roomData => {
    const card = createCard(roomData);
    cardsContainer.append(card);
  });
};

const onUpdateUserStatusInRoom = () => {
  socket.emit('UPDATE_STATUS', {activeRoomName, username});
};

const joinRoomDone = ({ roomMembers, roomName }) => {
  removeClass(gamePage, "display-none");
  addClass(roomsPage, "display-none");
  members = roomMembers;
  clearGameUserList();
  fillGameUserList();
  setActiveRoomName(roomName);
};

const updateUsersList = roomMembers => {
  members = roomMembers;
  clearGameUserList();
  fillGameUserList();
};

const updateReadyButton = () => {
  readyButton.textContent == 'READY' ? readyButton.textContent = 'NOT READY' : readyButton.textContent = 'READY';
};

const updateCountSecondsBeforeStartGame = (countValue) => {
  const gameTextElement = document.getElementById("game-text");
  const p = createTimerText(countValue);
  gameTextElement.replaceWith(p); 
};

const updateGameTime = (countValue) => {
  const gameTimerSpan = document.getElementById("game-timer");
  const span = createGameTimer(countValue);
  gameTimerSpan.replaceWith(span);
};

const startGame = ({gamers, countValue}) => {
  const gameTextElement = document.getElementById("game-text");
  const gamer = gamers.find(gamer => gamer.username === username);
  const { gameText } = gamer;
  const p = createText(gameText);
  gameTextElement.replaceWith(p);
  isGameStarted = true;
  const span = createGameTimer(countValue);
  gameArea.prepend(span);
};

const updateGameStatus = (gamer) => {
  const gameTextElement = document.getElementById("game-text");
  const { gameText, currentKeyPosition, process } = gamer;
  const inputedText = gameText.slice(0, currentKeyPosition);
  const restText = gameText.slice(currentKeyPosition);
  const p = createMarkedText(inputedText, restText);
  gameTextElement.replaceWith(p); 
  socket.emit("UPDATE_STATUS_BAR_IN_GAME", {activeRoomName, username, process});
};

const updateSingleGameStatus = ({ username, process }) => {
  const processValue = document.getElementById(username);
  processValue.style.width = `${process}%`;
  if (process == 100) {
    processValue.style.backgroundColor = 'green';
    socket.emit('TRY_FINISH_GAME', {activeRoomName, username});
  }
};

const finishGame = (finishedUsersArr) => {
  alert(finishedUsersArr.map((username, i) => ` ${i+1}. ${username} `));
  clearTimer();
  isGameStarted = false;
  socket.emit('LEAVE_ROOM', activeRoomName);
};

const onTryLeaveRoom = () => {
  socket.emit('TRY_LEAVE_ROOM', {activeRoomName, username});
};

const onLeaveRoom = (members) => {
  addClass(gamePage, "display-none");
  removeClass(roomsPage, "display-none");
  members = [...members];
  clearGameUserList();
  fillGameUserList();
  setActiveRoomName(null);
};

createButton.addEventListener('click', onCreateRoom);
leaveButton.addEventListener('click', onTryLeaveRoom);
readyButton.addEventListener('click', onUpdateUserStatusInRoom);
cardsContainer.addEventListener('click', onSelectRoom);
window.addEventListener('keypress', onKeyPress);

socket.on('UPDATE_ROOMS', updateRooms);
socket.on('EXIST_ROOM', existRoom);
socket.on('FULL_ROOM', fullRoom);
socket.on('JOIN_ROOM_DONE', joinRoomDone);
socket.on('UPDATE_USER_LIST', updateUsersList);
socket.on('UPDATE_READY_BUTTON', updateReadyButton);
socket.on('LOAD_ROOMS', updateRoomsList);
socket.on('TIMER_COUNTER', updateCountSecondsBeforeStartGame);
socket.on('GAME_TIMER_COUNTER', updateGameTime);
socket.on('START_GAME', startGame);
socket.on('KEY_PRESS_DONE', updateGameStatus);
socket.on('UPDATE_STATUS_BAR_IN_GAME_PROCESS', updateSingleGameStatus);
socket.on('FINISH_GAME', finishGame);
socket.on('LEAVE_ROOM_DONE', onLeaveRoom);

// If somebody refresh page
(() => { socket.emit('LOAD_ROOMS'); })();