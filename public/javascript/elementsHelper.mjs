import { createElement } from "./helper.mjs";

export const createCard = (roomData) => {
  const { roomName } = roomData;

  const container = createElement({
    tagName: "div",
    className: "card-container"
  });
  const title = createElement({ tagName: "div", className: "title" });
  const cardContent = createElement({
    tagName: "div",
    className: "card-content"
  });
  const h1 = createElement({ tagName: "h1" });
  const button = createElement({
    tagName: "button",
    attributes: { id: roomData.roomName }
  });
  const text = document.createTextNode("Join");

  title.append('Hello');
  h1.append(roomName);
  button.appendChild(text);
  cardContent.append(h1);
  cardContent.append(button);
  container.append(title);
  container.append(cardContent);

  return container;
};


export const createUserContainer = (member) => {
  const { username, isReady } = member;

  const container = createElement({
    tagName: "div",
    className: "user-container"
  });

  const status = createElement({
    tagName: "div",
    className: `status + ${isReady ? " ready" : ""}`
  });

  const nameArea = createElement({
    tagName: "div",
    className: "name-area"
  });

  const progressBar = createElement({
    tagName: "div",
    className: "progress-bar"
  });

  const progressBarValue = createElement({
    tagName: "div",
    className: "progress-bar-value",
    attributes: { id: username }
  });

  const userName = document.createTextNode(username);
  nameArea.append(userName);
  progressBar.append(progressBarValue);
  container.append(status);
  container.append(nameArea);
  container.append(progressBar);

  return container;
};

export const createTimerText = value => {
  const p = createElement({ tagName: "p", attributes: { id: 'game-text' } });
  const text = document.createTextNode(value);
  p.append(text);
  return p;
}

export const createText = gameText => {
  const p = createElement({ tagName: "p", attributes: { id: 'game-text' } });
  const text = document.createTextNode(gameText);
  p.append(text);
  return p;
}

export const createMarkedText = (inputedText, restText) => {
  const markElement = createElement({ tagName: "mark" });
  const text = document.createTextNode(inputedText);
  markElement.append(text);
  const p = createElement({ tagName: "p", attributes: { id: 'game-text' }  }); 
  const restGameText = document.createTextNode(restText);
  p.append(markElement);
  p.append(restGameText);
  return p;
}

export const createGameTimer = countValue => {
  const span = createElement({ tagName: "span", attributes: { id: 'game-timer' } });
  const text = document.createTextNode(`${countValue} seconds left`);
  span.append(text);
  return span;
}
