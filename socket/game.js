import { addRoom, updateUserStatusInRoom, getCurrentRoomName, getRooms, isExistRoom, getRoomMembers, addUserToRoom, deleteUserToRoom } from '../repositories/roomRepository';
import { MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME, TIMER_INTERVAL } from './config';
import { texts } from '../data';
import { selectKey, setFinishedGamer, setGamer, getGamers, getFinishedUsersArr, clearGamers } from '../repositories/statusGameRepository';
import { timerHelper } from '../helpers/timerHelper';

export default io => {
    io.on('connection', socket => { 
        
        socket.on('CREATE_ROOM', data => {
            const roomData = {...data};

            if (isExistRoom(roomData.roomName)) {
                socket.emit('EXIST_ROOM', roomData); 
                return;
            }
            addRoom({roomData, members: new Set()});
            io.emit('UPDATE_ROOMS', roomData);
        });

        socket.on('DELETE_ROOM', activeRoomName => {
            if (isExistRoom(activeRoomName)) {
                deleteRoom(activeRoomName);
                clearGamers();
                io.in(activeRoomName).disconnect();
                return;
            }
        });
    
        socket.on('JOIN_ROOM', roomData => {
            const { roomName, username } = roomData; 
            const prevRoomName = getCurrentRoomName(socket);
            if (roomName === prevRoomName) {
              return;
            }
            if (prevRoomName) {
              socket.leave(prevRoomName);
            }
            const members = getRoomMembers(roomName);

            if (members && members.length < MAXIMUM_USERS_FOR_ONE_ROOM) {
                socket.join(roomName, () => {
                    addUserToRoom(roomName, username);
                    const roomMembers = getRoomMembers(roomName);
                    io.in(roomName).emit("JOIN_ROOM_DONE", { roomMembers, roomName });
                });
                return;
            }
            socket.emit('FULL_ROOM', roomName);
        });

        socket.on('LOAD_ROOMS', () => {
            io.to(socket.id).emit('LOAD_ROOMS', [...getRooms().values()]);
        });


        // TODO: separate logic
        socket.on('UPDATE_STATUS', ({activeRoomName, username}) => {
            const roomMembers = Array.from(updateUserStatusInRoom(activeRoomName, username));
            io.in(activeRoomName).emit('UPDATE_USER_LIST', roomMembers);
            socket.emit('UPDATE_READY_BUTTON');
            if (!roomMembers.some(member => member.isReady === false)) {
                const gamers = roomMembers.map(member => {
                    const rand = Math.floor(Math.random() * texts.length);
                    member.gameText = texts[rand];
                    const gamer = setGamer(member);
                    return gamer;
                });

                const onDoneCallback = () => {
                    let countValue = SECONDS_FOR_GAME;
                    io.in(activeRoomName).emit('START_GAME', {gamers, countValue });

                    timerHelper(() => {
                        io.in(activeRoomName).emit('GAME_TIMER_COUNTER', countValue--);
                    }, TIMER_INTERVAL, SECONDS_FOR_GAME);
                } 

                let countValue = SECONDS_TIMER_BEFORE_START_GAME;

                timerHelper(() => {
                    io.in(activeRoomName).emit('TIMER_COUNTER', countValue--);
                }, TIMER_INTERVAL, SECONDS_TIMER_BEFORE_START_GAME, onDoneCallback);
                
            }
        }); 

        socket.on('KEY_PRESS', ({username, key}) => {
            const gamer = selectKey(username, key);
            socket.emit('KEY_PRESS_DONE', gamer);
        });

        socket.on('UPDATE_STATUS_BAR_IN_GAME', ({activeRoomName, username, process}) => {
            io.in(activeRoomName).emit('UPDATE_STATUS_BAR_IN_GAME_PROCESS', {username, process});
        });

        socket.on('TRY_FINISH_GAME', ({activeRoomName, username}) => {
            setFinishedGamer(username);
            const gamers = Array.from(getGamers());

            if (!gamers.some(member => member.isFinished === false)) {
                const finishedUsersArr = getFinishedUsersArr();
                io.in(activeRoomName).emit('FINISH_GAME', finishedUsersArr);
            }
        });

        socket.on('TRY_LEAVE_ROOM', ({activeRoomName, username}) => {
            socket.leave(activeRoomName);
            deleteUserToRoom(activeRoomName, username);
            const members = getRoomMembers(activeRoomName);
            socket.emit('LEAVE_ROOM_DONE', members);
            io.in(activeRoomName).emit('UPDATE_USER_LIST', members);
        });

        socket.on('LEAVE_ROOM', activeRoomName => {
            socket.leave(activeRoomName);
            const members = getRoomMembers(activeRoomName);
            socket.emit('LEAVE_ROOM_DONE', members);
        });
    });

    io.on('disconnect', socket => {

    });
};

