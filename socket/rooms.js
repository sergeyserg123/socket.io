import { isExistRoom } from "../helpers/roomHelper";

let rooms = [];

export default io => {
    io.on('connection', socket => { 
        
        socket.on('CREATE_ROOM', data => {
            const roomData = {...data};

            if (isExistRoom(roomData, rooms)) {
                socket.emit('EXIST_ROOM', roomData); 
                return;
            }

            rooms.push(roomData);
            socket.emit('UPDATE_ROOMS', roomData);
        });
    
        socket.on('JOIN_ROOM', roomName => {
            
            socket.emit('JOINED');
        });
    });

    io.on('disconnect', socket => {
        console.log('disconnected.');
    });
};

 