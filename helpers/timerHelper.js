export const timerHelper = (callback, delay, repetitions, onDoneCallback) => {
    let x = 0;
    let intervalID = setInterval(function () {

       callback();

       if (x++ === repetitions) {
           onDoneCallback();
           clearInterval(intervalID);
       }
    }, delay);
}